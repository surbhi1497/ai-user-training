**Deep Neural  Network(DNN)**
A Deep Neural Network is a part of Artificial Neural Network(ANN) and there are three types of layers of neurons in a neural network: the Input Layer, the Hidden Layer(s)(the layer(s) between input and output layer), and the Output Layer.
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/DNN.jpg
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/example.png

*Neuron : *
Thing that holds a number ranging from 0 to 1 known as its activation. Neurons lit up as activation is increased.
Neurons apply an Activation Function on the data to “standardize” the output coming out of the neuron.
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/Neuron.png

*Weight and Bias :*
When the inputs are transmitted between neurons, the weights are applied to the inputs along with the bias.
**x*W + b ->y**

The Activation Function of a node defines the output of that node given an input or set of inputs.

*Sigmoid Function :*
The main reason why we use sigmoid function is because it exists between (0 to 1). Therefore, it is especially used for models where we have to predict the probability as an output.
Graph below will illustrate it better :
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/sigmoid.png

*ReLU Function : *
ReLU stands for Rectified Linear Unit (ReLU), the activation function is responsible for transforming the summed weighted input from the node into the activation of the node or output for that input.
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/ReLU.jpg

*Cost Function : *
Accuracy of a hypothesis is cost function also known as mean squared error.
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/cost_function.png

Gradient Descent :
An optimization algorithm used to find the values of parameters of function(f) that minimizes the cost function.
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/gradient_descent.jpg
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/grad_descent2.jpg

*Back Propagation : *
A widely used algorithm in training feedforward neural networks for supervised learning. It is the practice of fine-tuning the weights of a neural net based on the error rate (i.e. loss) obtained in the previous epoch (i.e. iteration). Proper tuning of the weights ensures lower error rates, making the model reliable by increasing its generalization.
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/backpropagationjpg.jpg

*Stochastic Gradient Descent : *
An iterative method for optimizing an objective function with suitable smoothness properties.
https://gitlab.com/surbhi1497/ai-user-training/-/blob/master/summary_images/Stochastic_Gradient_Decent.png
The advantages of Stochastic Gradient Descent are:
- Efficiency.
- Ease of implementation (lots of opportunities for code tuning).

The disadvantages of Stochastic Gradient Descent include:
- SGD requires a number of hyperparameters such as the regularization parameter and the number of iterations.
- SGD is sensitive to feature scaling.






